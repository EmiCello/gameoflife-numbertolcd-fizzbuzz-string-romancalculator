import org.junit.Test;
import static org.junit.Assert.*;

public class StringCalculatorLogicTest {

    StringCalculatorLogic calculator = new StringCalculatorLogic();

    @Test
    public void doesContainsCorrectCharacters(){

        String input = "1,2,3,,5.67, ,.";
        String correctCharacters = "0 123456789,.";

        assertTrue(calculator.containsCorrectCharacters(input,correctCharacters));

    }

    @Test
    public void doesCheckMissingLastMarkOnLastPosition(){
        String input = "1,2,3,,5.67,";
        assertTrue(calculator.isMissisngNumberInLastPosition(input));
    }

    @Test
    public void doesMakeNewNumber(){
        String input = "1234,";
        String expectedOutput = "1234";
        assertEquals(expectedOutput, calculator.newNumber(input));
    }

    @Test
    public void doesMakeCorrectArray(){
        String input = "1, ,2,3,4,5";
        String[] tab = new String[]{"1", " ", "2", "3", "4", "5"};
        assertArrayEquals(tab,calculator.tabOfNumbers(input));
    }

    @Test
    public void doesUpdateTheTab(){
        String[] input = new String[]{"1","","2","3","4"," ","5"};
        String[] tab = new String[]{"1", "0", "2", "3", "4","0", "5"};
        assertArrayEquals(tab,calculator.updatedTabOfNumber(input));
    }

    @Test
    public void doesCountainEmpty(){
        String[] input = new String[]{"1","","2","3","4"," ","5"};
        assertTrue(calculator.containsEmpty(input));
    }

    @Test
    public void doesMakeCorrectSum(){
        String input = "1, ,2,3.5,,30";
        String stringSum = "36.5";
        assertEquals(stringSum, calculator.add(input));
    }
}
