import org.junit.Test;
import java.util.Random;
import static org.junit.Assert.*;

public class GameOfLifeTest {

    GameOfLifeLogic gameOfLifeLogic = new GameOfLifeLogic();
    
    @Test
    public void doesCalculateCorrectNextGenerationFromDeathCell() {
        boolean[][] generation = prepareGeneration(4, 8);
        assertTrue(gameOfLifeLogic.checkingForDeadCell(generation, 1, 3));
    }

    @Test
    public void doesCalculateCorrectNextGenerationFromLivingCell() {
        boolean[][] generation = prepareGeneration(4, 8);
        assertTrue(gameOfLifeLogic.checkingForLivingCell(generation, 1, 4));
    }

    @Test
    public void calculateCorrectAmountOfBorderer() {
        boolean[][] generation = prepareGeneration(4, 8);
        assertEquals(2, gameOfLifeLogic.checkedSumOfLivingBorderer(generation,
                2, 4));
    }

    @Test
    public void doesCorrectBordererChecking() {
        boolean[][] tmp1 = new boolean[1][2];
        tmp1[0][0] = true;
        tmp1[0][1] = false;
        assertTrue(gameOfLifeLogic.existingBorderer(tmp1, 0, 0));
        assertFalse(gameOfLifeLogic.existingBorderer(tmp1, 1, 2));
    }

    @Test
    public void doesCreateFirstGeneration() {
        Random generator = new Random();
        assertNotNull(gameOfLifeLogic.generateFistGeneration(generator.nextInt(10), generator.nextInt(20)));
    }

    private static boolean[][] prepareGeneration(int row, int column) {
        boolean[][] firstGeneration = new boolean[row][column];
        for (int i = 0; i < firstGeneration.length; i++) {
            for (int j = 0; j < firstGeneration[i].length; j++) {
                if ((i == 1 && j == 4) || (i == 2 && j == 3) || (i == 2 && j == 4)) {
                    firstGeneration[i][j] = true;
                } else {
                    firstGeneration[i][j] = false;
                }
            }
        }
        return firstGeneration;
    }
}


