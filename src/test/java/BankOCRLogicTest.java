import org.apache.commons.io.IOUtils;
import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;


public class BankOCRLogicTest {

    BankOCRLogic bankOCRLogic = new BankOCRLogic();


    @Test
    public void isCorrectConversionTest0()throws IOException{
        String path = preparePath("Test1").getAbsolutePath();
        assertEquals(Arrays.asList("000000000",""), 
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                              bankOCRLogic.doActualAccountNumbers(path)[0][1]));
    }

    @Test
    public void isCorrectConversionTest1()throws IOException{
        String path = preparePath("Test2").getAbsolutePath();
        assertEquals(Arrays.asList("111111111","ERR"), 
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                              bankOCRLogic.doActualAccountNumbers(path)[0][1]));
    }
    
    @Test
    public void isCorrectConversionTest8()throws IOException{
        String path = preparePath("Test8").getAbsolutePath();
        assertEquals(Arrays.asList("888888888","ERR"), 
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                              bankOCRLogic.doActualAccountNumbers(path)[0][1]));
    }

    @Test
    public void isCorrectConversionTest9()throws IOException{
        String path = preparePath("Test9").getAbsolutePath();
        assertEquals(Arrays.asList("999999999","ERR"), 
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                              bankOCRLogic.doActualAccountNumbers(path)[0][1]));
    }

    @Test
    public void isCorrectConversionTestAll9Numbers()throws IOException{
        String path = preparePath("All9Numbers").getAbsolutePath();
        assertEquals(Arrays.asList("123456789",""), 
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                              bankOCRLogic.doActualAccountNumbers(path)[0][1]));
    }

    @Test
    public void isCorrectConversionTestFewRows()throws IOException{
        String path = preparePath("FewRows").getAbsolutePath();

        assertEquals(Arrays.asList("013619245","ERR"),
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[0][0],
                        bankOCRLogic.doActualAccountNumbers(path)[0][1]));

        assertEquals(Arrays.asList("457508000",""),
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[1][0],
                        bankOCRLogic.doActualAccountNumbers(path)[1][1]));

        assertEquals(Arrays.asList("95750800?","ILL"),
                Arrays.asList(bankOCRLogic.doActualAccountNumbers(path)[2][0],
                        bankOCRLogic.doActualAccountNumbers(path)[2][1]));
    }


    @Test
    public void hasValidChecksum()throws IOException{
        String path = preparePath("HasValidChecksum").getAbsolutePath();
        String[] readFile = BankOCRLogic.readFile(path);
        String[][] tab = BankOCRLogic.putReadFileIntoMultiArray(readFile);
        Object[] numbers = BankOCRLogic.allNumbers(tab);
        List<String> listOfNumbers = BankOCRLogic.listOfActualAccountNumbers(numbers, tab);
        
        boolean[] expectedValidChecksum = new boolean[] {true, false, true, false};
        
        for(int i = 0; i < listOfNumbers.size(); i++)
            assertEquals("Błąd na pozycji " + String.valueOf(i),
                    expectedValidChecksum[i], bankOCRLogic.hasValidChecksum(listOfNumbers.get(i)));
    }
 
    
    private static File preparePath(String name) throws IOException {
        InputStream in = BankOCRMain.class.getClassLoader().getResourceAsStream(name);
        File temp = File.createTempFile("temp-file-name", ".tmp");
        temp.deleteOnExit();
        FileOutputStream out = new FileOutputStream(temp);
        IOUtils.copy(in, out);
        return temp;
    }
}

