import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ChlebikConfigChangelog {

    private StringBuilder content = new StringBuilder();
    private FileReader filereader;

    public void loadFile(String fileName) throws FileNotFoundException
    { 
        filereader = new FileReader(fileName);
    }

    public String readFile() throws IOException {

        String s;
        BufferedReader br = new BufferedReader(filereader);

        while((s = br.readLine()) != null)
        {
            System.out.println(s);
            content.append(s + "\n");
        }
        filereader.close();

        if( content.length() > 0 )
        {
            return content.toString();
        }
        else
        {
            throw new IOException();
        }
    }
}
