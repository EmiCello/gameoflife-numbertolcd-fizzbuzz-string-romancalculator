import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class BankOCRLogic {

    private static final Map<String, Integer> MAP = new HashMap<String, Integer>() {
        {
            put("   " +
                    "  |" +
                    "  |", 1);

            put(" _ " +
                    " _|" +
                    "|_ ", 2);

            put(" _ " +
                    " _|" +
                    " _|", 3);

            put("   " +
                    "|_|" +
                    "  |", 4);

            put(" _ " +
                    "|_ " +
                    " _|", 5);

            put(" _ " +
                    "|_ " +
                    "|_|", 6);

            put(" _ " +
                    "  |" +
                    "  |", 7);

            put(" _ " +
                    "|_|" +
                    "|_|", 8);

            put(" _ " +
                    "|_|" +
                    " _|", 9);

            put(" _ " +
                    "| |" +
                    "|_|", 0);
        }
    };


    public String[][] doActualAccountNumbers(String filePath) throws IOException {
        String[] readFile = readFile(filePath);
        String[][] tab = putReadFileIntoMultiArray(readFile);
        Object[] numbers = allNumbers(tab);
        List<String> listOfNumbers = listOfActualAccountNumbers(numbers, tab);
        String[][] tabOfNumbers = prepareMapOfActuallAccountNumbers(listOfNumbers);
        return tabOfNumbers;
    }


    public static String[] readFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        List<String> allReadLines = Files.readAllLines(path);
        return allReadLines.toArray(new String[allReadLines.size()]);
    }
 

    public static String[][] putReadFileIntoMultiArray(String[] readFile) {

        int lengthSingleTab = readFile[0].length() / 3;
        String[][] tab = new String[readFile.length][lengthSingleTab];

        for (int i = 0; i < readFile.length; i++) {
            int x = 0;
            for (int j = 0; j < lengthSingleTab; j++) {
                if (readFile[i].substring(x, x + 3).equals("") || readFile[i].substring(x, x + 3).equals(" ")) {
                    tab[i][j] = new String("*");
                    x += 3;
                } else {
                    tab[i][j] = new String(readFile[i].substring(x, x + 3));
                    x += 3;
                }
            }
        }
        return tab;
    }

    public static Object[] allNumbers(String[][] tab) {

        List<Object> listOfNumbers = new ArrayList<>();

        int amountOfRows = 0;
        while (amountOfRows < tab.length) {
            int localLimit = amountOfRows + 3;
            for (int j = 0; j < tab[0].length; j++) {
                String digitToCompare = "";
                while (amountOfRows < localLimit) {
                    digitToCompare += tab[amountOfRows][j];
                    amountOfRows++;
                }
                
                if (MAP.containsKey(digitToCompare)) {
                    listOfNumbers.add(MAP.get(digitToCompare));
                } else {
                    listOfNumbers.add("?");
                }

                if (j < tab[0].length - 1) {
                    amountOfRows -= 3;
                }
                if (j == tab[0].length - 1) {
                    amountOfRows++;
                }
            }
        }

        Object[] allNumbers = new Object[listOfNumbers.size()];
        for (int y = 0; y < allNumbers.length; y++) {
            allNumbers[y] = listOfNumbers.get(y);
        }
        return allNumbers;
    }


    public static List<String> listOfActualAccountNumbers(Object[] tab, String[][] tab2) {

        List<String> list = new ArrayList<>();

        int i = 0;
        while (i < tab.length) {
            String single = "";
            int counter = 0;
            while (counter < tab2[0].length) {
                single += tab[i];
                counter++;
                i++;
            }
            if (counter == tab2[0].length) {
                list.add(single);
            }
        }
        return list;
    }


    String[][] prepareMapOfActuallAccountNumbers(List<String> list) throws IOException {
        
        String[][] tmpTab = new String[list.size()][2];
        
        for(int i = 0; i < tmpTab.length; i++){
            boolean doesCountainsCorrectCharakters = doesCointainsCorrectCharakters(list.get(i));
            if(doesCountainsCorrectCharakters){
                boolean hasValidChecksum = hasValidChecksum(list.get(i));
                if(hasValidChecksum){
                    tmpTab[i][0] = list.get(i);
                    tmpTab[i][1] = "";
                }else{
                    tmpTab[i][0] = list.get(i);
                    tmpTab[i][1] = "ERR";
                }
            }else{
                tmpTab[i][0] = list.get(i);
                tmpTab[i][1] = "ILL";
            }
        }
        return tmpTab;
    }


    boolean doesCointainsCorrectCharakters(String s) {

        String permittionMarks = "0123456789";
        
        int checkSum = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!permittionMarks.contains(String.valueOf(s.charAt(i)))) {
                checkSum++;
            }
        }

        if (checkSum != 0) {
            return false;
        } else {
            return true;
        }
    }

    boolean hasValidChecksum(String s) throws IOException {
        int sum = 0;
        int x = 1;
        for (int i = s.length(); i > 0; i--) {
            sum += Integer.parseInt(s.substring(i - 1, i)) * x;
            x++;
        }
        
        if (sum % 11 == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    void printActuallAccountNumbers(String[][] tab){        
        for(String[] singleTab : tab){
            for(String singleRow : singleTab){
                System.out.print(singleRow + " ");
            }
            System.out.print("\n");
        }        
    }
}