import java.util.HashMap;
import java.util.Map;

public class DigitsLogic {

    private static final Map<Integer, String[]> MAP = new HashMap<Integer, String[]>() {
        {
            put(1, new String[] {
                    "   ",
                    "  |",
                    "  |"
            });
            put(2, new String[] {
                    " _ ",
                    " _|",
                    "|_ "
            });
            put(3, new String[] {
                    " _ ",
                    " _|",
                    " _|"
            });
            put(4, new String[] {
                    "   ",
                    "|_|",
                    "  |"
            });
            put(5, new String[] {
                    " _ ",
                    "|_ ",
                    " _|"
            });
            put(6, new String[] {
                    " _ ",
                    "|_ ",
                    "|_|"
            });
            put(7, new String[] {
                    " _ ",
                    "  |",
                    "  |"
            });
            put(8, new String[] {
                    " _ ",
                    "|_|",
                    "|_|"
            });
            put(9, new String[] {
                    " _ ",
                    "|_|",
                    " _|"
            });
            put(0, new String[] {
                    " _ ",
                    "| |",
                    "|_|"
            });
        }
    };


    void toLCD(String[][] tab, String digits){

       for(int i  = 0; i < tab.length; i++){
           for(int j = 0; j < digits.length(); j++){
               System.out.print(tab[i][j]);
           }
           System.out.println("");
       }

    }


    String[][] numberInTab(String digits){

        String[][] tab = new String[3][digits.length()];

        for(int j = 0; j < digits.length(); j++){

            int identifiedNumber = Integer.parseInt(identifiedDigitsToCompare(digits)[j]);
            String[] singleDigit = MAP.get(identifiedNumber);

            for(int i = 0; i < 3; i++){
                tab[i][j] = singleDigit[i];
            }

        }

        return tab;
    }

    String[] identifiedDigitsToCompare(String digits){

        String[] tab = new String[digits.length()];

        for(int i = 0; i < digits.length(); i++){
            tab[i] = String.valueOf(digits.charAt(i));
        }
        return tab;
    }


}


