import java.util.ArrayList;
import java.util.List;

public class RomanCalculator {



    int sum(String first, String second){

        int sum;

        if(doesContainsCorrectAmountOfIXC(first) && doesCountainsCorrectAmountOfVLD(first)
                && doesContainsCorrectAmountOfIXC(second) && doesCountainsCorrectAmountOfVLD(second)){

            int firstNumber = convertedRomanNumber(convertedRomanNumbersToSingleInt(first));
            int secondNumber = convertedRomanNumber(convertedRomanNumbersToSingleInt(second));

            sum = firstNumber + secondNumber;
            return sum;

        }else{

            System.out.println("Incorrect input");
            return 0;

        }
    }


    boolean doesCountainsCorrectAmountOfVLD(String s){
        if(s.contains("VV") || s.contains("LL") || s.contains("DD")){
            return false;
        }
        return true;
    }


    boolean doesContainsCorrectAmountOfIXC(String s){

      if(s.contains("IIII") || s.contains("XXXX") || s.contains("CCCC")){
          return false;
      }
      return true;
    }




    int convertedRomanNumber(int[] tab) {

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < tab.length; i++) {

                int first = 0;
                int second = 0;

                if(i == tab.length - 1){
                    first = tab[i];
                }else{
                    first = tab[i];
                    second = tab[i + 1];
                }

                if (first >= second) {
                    list.add(first);
                }
                if (first < second) {
                    list.add(second - first);
                    i++;
                }

            }

        int sum = 0;
        for(Integer i : list){
            sum += i;
        }
        return sum;
    }


    int[] convertedRomanNumbersToSingleInt(String number){

        int[] tmpTab = new int[number.length()];

        for(int i = 0; i < number.length(); i++){
            String tmpNumber = String.valueOf(number.charAt(i));
            switch (tmpNumber){
                case "I" :
                    tmpTab[i] = 1;
                    break;
                case "V" :
                    tmpTab[i] = 5;
                    break;
                case "X" :
                    tmpTab[i] = 10;
                    break;
                case "L" :
                    tmpTab[i] = 50;
                    break;
                case "C" :
                    tmpTab[i] = 100;
                    break;
                case "D" :
                    tmpTab[i] = 500;
                    break;
                case "M" :
                    tmpTab[i] = 1000;
                    break;
            }

        }
        return tmpTab;
    }
}
