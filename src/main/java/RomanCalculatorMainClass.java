
public class RomanCalculatorMainClass {

    public static void main(String[] args) {

        RomanCalculator romanCalculator = new RomanCalculator();

        String firstNumber = "XLIII";
        String secondNumber = "XX";

        System.out.println(romanCalculator.sum(firstNumber, secondNumber));

    }

}
