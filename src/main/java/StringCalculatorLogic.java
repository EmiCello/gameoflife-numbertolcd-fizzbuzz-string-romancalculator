import java.util.ArrayList;
import java.util.List;

public class StringCalculatorLogic {

    String add(String number) {

        String correctCharacters = "0 123456789.,";
        double sum = 0;

        if(containsCorrectCharacters(number, correctCharacters)){

            if (isMissisngNumberInLastPosition(number)) {
                number = newNumber(number);
            }

            String[] tab = tabOfNumbers(number);

            if (containsEmpty(tab)) {
                tab = updatedTabOfNumber(tab);
            }

            for (String s : tab) {
                sum += Double.parseDouble(s);
            }

            return String.valueOf(sum);
        }

        System.out.println("Number expected but foud not allowed mark");
        return null;
    }

    boolean containsCorrectCharacters(String number, String c){
        int x = 0;
        for(int i = 0; i < number.length(); i++){
            String tmp = number.substring(i, i + 1);
            if(!c.contains(tmp)){
                x++;
            }
        }
        if(x > 0){
            return false;
        }
        return true;

    }

    boolean isMissisngNumberInLastPosition(String number) {
        if ((number.substring(number.length() - 1).equals(","))) {
            System.out.println("Number expected but EOF found");
            return true;
        }
        return false;
    }

    String newNumber(String number) {
        StringBuilder sb = new StringBuilder(number);
        number = sb.delete(sb.length() - 1, sb.length()).toString();
        return number;
    }

    String[] tabOfNumbers(String number) {
        String[] tab = number.split(",");
        return tab;
    }

    String[] updatedTabOfNumber(String[] tab) {

        List<String> list = new ArrayList<>();

        for (String a : tab) {
            if (a.isEmpty() || a.equals(" ")) {
                list.add("0");
            } else {
                list.add(a);
            }
        }

        String[] tmpTab = new String[list.size()];
        for (int i = 0; i < tmpTab.length; i++) {
            tmpTab[i] = list.get(i);
        }
        return tmpTab;
    }

    boolean containsEmpty(String[] number) {
        int x = 0;
        for (String s : number) {
            if (s.equals("") || s.equals(" ")) {
                x++;
            }
        }
        if (x > 0) {
            return true;
        }
        return false;
    }
}
