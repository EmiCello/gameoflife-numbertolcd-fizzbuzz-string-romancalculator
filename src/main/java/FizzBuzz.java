
import java.util.List;

public class FizzBuzz {   
     
    
    public static void main(String[] args) throws InterruptedException {
        
        FizzBuzzLogicAlternative fizzBuzzLogicAlternative = new FizzBuzzLogicAlternative();
        printPreparedGame(fizzBuzzLogicAlternative.playBizzBuzzGame());
        
    }
    
    
   private static void printPreparedGame(List<String> list){        
        list.forEach(System.out::println);
   }  
 
    
}


