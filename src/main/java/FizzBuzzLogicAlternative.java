import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FizzBuzzLogicAlternative {
    
    List<String> playBizzBuzzGame(){
        List<Integer> listOfNumbers = prepareListOfNumbers();
        return prepareResultList(listOfNumbers);
    }
    

    List<Integer> prepareListOfNumbers(){
        return IntStream.rangeClosed(1, 100).boxed().collect(Collectors.toList());
    }

    private List<String> prepareResultList(List<Integer> list){
        return list.stream().map(i -> mapToString(i)
                                 ).collect(Collectors.toList());
    }

    private String mapToString(int singleNnumber){
        if(isMultiplesOfThreeAndFive(singleNnumber)){
            return "BizzBuzz";
        }else if(isMultiplesOfThree(singleNnumber)){
            return "Bizz";
        }else if(isMultiplesOfFive(singleNnumber)){
            return "Buzz";
        }else{
            return String.valueOf(singleNnumber);
        }
    }
    

    private boolean isMultiplesOfThreeAndFive(int number){
        return number % 5 == 0 && number % 3 ==0;
    }

    private boolean isMultiplesOfThree(int number){
        return number % 3 ==0;
    }

    private boolean isMultiplesOfFive(int number){
        return number % 5 ==0;
    }
}
