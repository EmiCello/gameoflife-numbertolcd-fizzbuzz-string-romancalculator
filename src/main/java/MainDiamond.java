import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainDiamond {

    public static void main(String[] args) {


        String alfabet = new String("abcdefghijklłmnoprstuwyz");
        char letter = JOptionPane.showInputDialog("Podaj literę").charAt(0);
        int centerOfDiamond = alfabet.indexOf(letter);
        int amonutOfRows = (centerOfDiamond * 2) + 1;

        String[][] diamondTab = new String[amonutOfRows][amonutOfRows];

        int a = 0;
        int b = 0;
        int x = centerOfDiamond;
        int y = x;
        int c = 0;


        for(int i = 0; i < diamondTab.length; i++){

            for(int j = 0; j < diamondTab.length; j++){

                if(a > 0 && a <= diamondTab.length){

                    if (j == a || j == b) {
                        diamondTab[i][j] = new String(alfabet.substring(c, c + 1));
                    } else {
                        diamondTab[i][j] = new String(" ");
                    }

                }

                if(x >= 0) {

                    if (j == x || j == y) {
                        diamondTab[i][j] = new String(alfabet.substring(i, i + 1));
                        if(x == 0){
                            a = x + 1;
                            b = y - 1;
                            c = i - 1;
                        }
                    }
                    else {
                        diamondTab[i][j] = new String(" ");
                    }

                }

            }
            if(x < 0){
                a++;
                b--;
                c--;
            }
            x--;
            y++;

        }

        for(int i = 0; i < diamondTab.length; i++){
            for(int j = 0; j < diamondTab.length; j++){
                System.out.print(diamondTab[i][j].toString() + " ");
            }
            System.out.println(" ");
        }
    }
}
