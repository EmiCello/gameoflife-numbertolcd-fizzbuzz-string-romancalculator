
public class GameOfLife {
    /**
     * @see <"http://codingdojo.org/kata/GameOfLife/">
     * @author Emilia Traczyk
     * @version 1.0
     */
    
    public static void main(String[] args) {               
        
        GameOfLifeLogic gameOfLifeLogic = new GameOfLifeLogic();
        boolean[][] firstGeneration = gameOfLifeLogic.prepareFirstGeneration(4,8);
        gameOfLifeLogic.printCellGrid(firstGeneration);        
        System.out.println("\n"); 
        boolean[][] secondGeneration = gameOfLifeLogic.prepareNextCellGrid(firstGeneration);
        gameOfLifeLogic.printCellGrid(secondGeneration);
        
    }

}

    
        
    


