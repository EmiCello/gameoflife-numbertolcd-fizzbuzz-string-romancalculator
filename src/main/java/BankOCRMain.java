import java.io.IOException;
import java.util.List;

public class BankOCRMain {

    public static void main(String[] args) throws IOException {

        String filePath = "C:\\Users\\Emilia\\Desktop\\Bank OCR.txt";
        BankOCRLogic bankOCRLogic = new BankOCRLogic();   
        bankOCRLogic.printActuallAccountNumbers(bankOCRLogic.doActualAccountNumbers(filePath));
    }

    private static void printTheActualAccountNumbers(List<String> listOfActualAccountNumbers){
        for(String s : listOfActualAccountNumbers){
            System.out.println(s);
        }
    }
    
   
}
