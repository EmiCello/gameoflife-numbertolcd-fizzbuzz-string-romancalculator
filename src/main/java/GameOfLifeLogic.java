import java.util.Random;

public class GameOfLifeLogic {

    /**
     * This method prepare next generation.
     * @param currentCellGrid is actuall state of all cells.=
     * @return next state of all cells (next generation).
     */
    public static boolean[][] prepareNextCellGrid(boolean[][] currentCellGrid) {

        boolean[][] nextCellGrid = new boolean[currentCellGrid.length][currentCellGrid[0].length];

        for (int i = 0; i < nextCellGrid.length; i++) {
            for (int j = 0; j < nextCellGrid[i].length; j++) {
                if (!(i == 0 || j == 0 || i == currentCellGrid.length - 1 || j == currentCellGrid[i].length - 1)) {
                    nextCellGrid[i][j] = (currentCellGrid[i][j]) ?
                            checkingForLivingCell(currentCellGrid, i, j) : checkingForDeadCell(currentCellGrid, i, j);
                } else {
                    nextCellGrid[i][j] = false;
                }
            }
        }
        return nextCellGrid;
    }

    /**
     * This method calculate next state of single living cell.
     * @param tab    multi array
     * @param row    position of checing cell
     * @param column position of checking cell
     * @return next state.
     */
    protected static boolean checkingForLivingCell(boolean[][] tab, int row, int column) {
        int sumOfLivingBorderer = checkedSumOfLivingBorderer(tab, row, column);
        return !(sumOfLivingBorderer < 2 || sumOfLivingBorderer > 3);
    }

    /**
     * This method calculate next state of single death cell.
     * @param tab    multi array
     * @param row    position of checing cell
     * @param column position of checking cell
     * @return next state.
     */
    protected static boolean checkingForDeadCell(boolean[][] tab, int row, int column) {
        int sumOfLivingBorderer = checkedSumOfLivingBorderer(tab, row, column);
        return sumOfLivingBorderer == 3;
    }

    /**
     * This method calculate sum of living bodreres for single cell.
     * @param tab    multi array
     * @param row    position of checing cell
     * @param column position of checking cell
     * @return sum of living borderer.
     */
    protected static int checkedSumOfLivingBorderer(boolean[][] tab, int row, int column) {
        int sum = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (!(i == 0 && j == 0) && (existingBorderer(tab, row + i, column + j)
                        && tab[row + i][column + j])) {
                    sum++;
                }
            }
        }
        return sum;
    }

    /**
     * This method checks existing of potencial cell.
     * @param tab    multi array
     * @param row    position of checing cell
     * @param column position of checking cell
     * @return boolean true - if borderer exists, false - if borderer doesn't exist.
     */
    protected static boolean existingBorderer(boolean[][] tab, int row, int column) {
        return !(row < 0 || row >= tab.length || column < 0 || column >= tab[0].length);
    }

    protected static boolean[][] generateFistGeneration(int row, int column) {
        boolean[][] cellGrid = new boolean[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                cellGrid[i][j] = randomCellState();
            }
        }
        return cellGrid;
    }

    protected static boolean randomCellState() {
        Random generator = new Random();
        return generator.nextBoolean();
    }

    

    /**
     * This method prepers first generation of cellGrid.
     * @param row    amount of multi array
     * @param column lenght of each singular array
     * @return boolean[][] firstGeneration with living and death cells.
     */

    protected static boolean[][] prepareFirstGeneration(int row, int column) {
        boolean[][] firstGeneration = new boolean[row][column];
        for (int i = 0; i < firstGeneration.length; i++) {
            for (int j = 0; j < firstGeneration[i].length; j++) {
                if ((i == 1 && j == 4) || (i == 2 && j == 3) || (i == 2 && j == 4) || (i == 0 && j == 0) || (i == 3 && j == 2) || (i == 1 && j == 7)) {
                    firstGeneration[i][j] = true;
                } else {
                    firstGeneration[i][j] = false;
                }
            }
        }
        return firstGeneration;
    }

    /**
     * This method print prepared boolean[][].
     * @param cellGrid filled multi array
     */
    protected static void printCellGrid(boolean[][] cellGrid) {
        for (int i = 0; i < cellGrid.length; i++) {
            for (int j = 0; j < cellGrid[i].length; j++) {
                if (cellGrid[i][j]) {
                    System.out.print("*");
                } else {
                    System.out.print(".");
                }
            }
            System.out.println("");
        }
    }

}
    
